package com.example.javaapiclient.enums;

/**
 * 字段类型枚举
 */
public enum ESFieldType {
	Text, Integer, Long, Date, Float, Double, Boolean, Object, Nested, Ip, Attachment, Keyword
}
