package com.example.javaapiclient.enums;

/**
 * 聚合类型枚举
 */
public enum ESAggregationType {
	count,avg,max,min,sum,stats,terms,cardinality,range,dateRange,histogram,dateHistogram,percentiles,percentileRanks
}
