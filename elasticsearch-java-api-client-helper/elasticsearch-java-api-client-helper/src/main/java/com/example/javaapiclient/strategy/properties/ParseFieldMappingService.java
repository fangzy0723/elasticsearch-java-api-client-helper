package com.example.javaapiclient.strategy.properties;

import com.example.javaapiclient.annotations.ESField;

import java.util.Map;

/**
 * 使用策略模式根据枚举值解析字段mapping
 */
public interface ParseFieldMappingService {

    Map<String,Object> getFieldMapping(ESField esField);
}
