package com.example.javaapiclient.entity.common;

import com.example.javaapiclient.enums.ESAggregationType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AggregationParamEntity implements Serializable {

    //聚合名称
    private String name;

    //聚合字段名
    private String field;

    //聚合类型
    private ESAggregationType type;

    //子聚合
    private List<AggregationParamEntity> subAgg;

    //聚合结果返回条数
    private Integer size = 10;

    //聚合区间间隔
    private Double interval = 1000.0;

    //日期范围统计，开始时间
    private LocalDate startDate;

    //日期范围统计，结束时间
    private LocalDate endDate;

    //饼状图百分位值区间
    private List<Double> percentileRanksValues;
}
