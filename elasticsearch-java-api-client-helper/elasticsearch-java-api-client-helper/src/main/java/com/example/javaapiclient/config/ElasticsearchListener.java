package com.example.javaapiclient.config;

import com.example.javaapiclient.annotations.ESDocument;
import com.example.javaapiclient.service.ElasticsearchIndexService;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 用于扫描ESDocument注解的类，并自动创建索引mapping
 * 启动时调用
 **/
@Slf4j
@Component
public class ElasticsearchListener implements ApplicationListener<ApplicationReadyEvent> {

    @Resource
    private ElasticsearchIndexService elasticsearchIndexService;

    /**
     * 扫描ESDocument注解的类，并自动创建索引mapping
     * @param applicationReadyEvent
     */
    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        log.info("【onApplicationEvent 开始执行】");
        if(applicationReadyEvent.getApplicationContext().getParent() != null){//解決二次调用问题
            return;
        }
        Map<String, Object> beans = applicationReadyEvent.getApplicationContext().getBeansWithAnnotation(ESDocument.class);
        beans.forEach((beanName, bean)->{
            try {
                if(!elasticsearchIndexService.exists(bean.getClass())){
                    elasticsearchIndexService.createIndex(bean.getClass());
                }
            } catch (Exception e) {
                log.error("创建索引不成功",e);
            }
        });
    }
}
