package com.example.javaapiclient.strategy.templates;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service(value = "dynamicTemplatesIp")
public class IPDynamicTemplatesServiceImpl implements DynamicTemplatesService {

    @Override
    public Map<String, Object> getDynamicTemplates() {
        Map<String, Object> map = new HashMap<>();

        Map<String, Object> mapMapping = new HashMap<>();
        mapMapping.put("type","ip");


        Map<String, Object> mapName = new HashMap<>();
        mapName.put("match_mapping_type","string");
        mapName.put("match","ip_*");
        mapName.put("mapping",mapMapping);

        map.put("strings_as_ip",mapName);
        return map;
    }
}
