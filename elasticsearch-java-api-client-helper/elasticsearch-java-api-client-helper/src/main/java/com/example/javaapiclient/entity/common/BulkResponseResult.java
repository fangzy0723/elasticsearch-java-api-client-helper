package com.example.javaapiclient.entity.common;

import co.elastic.clients.elasticsearch.core.bulk.OperationType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;

/**
 * 批量操作返回结果实体
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BulkResponseResult implements Serializable {

    private static final long serialVersionUID = 547402260621429004L;
    //操作类型
    private OperationType opType;

    //索引名
    private String indexName;

    //数据id（有值可认为操作成功）
    private String id;

    //返回结果
    private String result;

    //是否失败 false:没有失败  true：操作失败
    private boolean failedFlag;

    //失败原因
    private String failureMessage;
    private int status;
    private Long primaryTerm;
    private Long seqNo;
    private Long version;
    private Boolean forcedRefresh;


}
