package com.example.javaapiclient.entity;

import com.example.javaapiclient.annotations.ESDocument;
import com.example.javaapiclient.annotations.ESField;
import com.example.javaapiclient.entity.common.BaseEsEntity;
import com.example.javaapiclient.enums.ESAnalyzerType;
import com.example.javaapiclient.enums.ESFieldType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Component
@ESDocument(indexName = "test_index_auto_user_info")
public class UserInfoEntity extends BaseEsEntity implements Serializable {

    private static final long serialVersionUID = 5107963788245336298L;

    @ESField(name = "user_name",type = ESFieldType.Text,searchAnalyzer = ESAnalyzerType.IK_MAX_WORD,analyzer=ESAnalyzerType.IK_MAX_WORD)
    private String userName;

    @ESField(name = "user_age",type = ESFieldType.Integer)
    private Integer userAge;

    @ESField(name = "birth_date",type = ESFieldType.Date,format="yyyy-MM-dd'T'HH:mm:ss.SSSSSSSSS")
    private LocalDateTime birthDate;

    @ESField(name = "text_completion",type = ESFieldType.Text,completion = true,searchAsYouType = true)
    private String textCompletion;
}
