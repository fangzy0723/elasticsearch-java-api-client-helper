package com.example.javaapiclient.enums;

import java.util.Arrays;

/**
 * 常用分词器类型
 **/
public enum ESAnalyzerType {
    STANDARD("standard"),
    SIMPLE("simple"),
    WHITESPACE("whitespace"),
    KEYWORD("keyword"),
    ENGLISH("english"),
    IK_MAX_WORD("ik_max_word"),//中文友好
    IK_SMART("ik_smart"),//中文友好
    ;

    private String code;

    ESAnalyzerType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public static ESAnalyzerType getEnumByCode(String code) {
        return Arrays.stream(ESAnalyzerType.values()).filter(k-> k.getCode().equals(code)).findFirst().orElse(null);
    }
}
