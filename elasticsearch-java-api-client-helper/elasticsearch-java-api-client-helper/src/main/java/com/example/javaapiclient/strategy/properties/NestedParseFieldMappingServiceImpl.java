package com.example.javaapiclient.strategy.properties;

import com.example.javaapiclient.annotations.ESField;
import com.example.javaapiclient.utils.ElasticSearchHelpUtils;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.Map;

@Service(value = "parseFieldMappingNested")
public class NestedParseFieldMappingServiceImpl implements ParseFieldMappingService {

    @Resource
    private ElasticSearchHelpUtils elasticSearchHelpUtils;

    @Override
    public Map<String, Object> getFieldMapping(ESField esField) {
        Map<String, Object> map = new HashMap<>();
        map.put("type",esField.type().toString().toLowerCase());
        Class<?>[] classes = esField.childType();
        if (classes.length > 0){
            map.put("properties",elasticSearchHelpUtils.getProperties(classes[0]));
        }
        return map;
    }
}
