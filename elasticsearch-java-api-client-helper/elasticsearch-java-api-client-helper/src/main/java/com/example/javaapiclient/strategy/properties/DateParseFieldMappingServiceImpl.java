package com.example.javaapiclient.strategy.properties;

import com.example.javaapiclient.annotations.ESField;
import com.example.javaapiclient.utils.EmptyUtil;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service(value = "parseFieldMappingDate")
public class DateParseFieldMappingServiceImpl implements ParseFieldMappingService {
    @Override
    public Map<String, Object> getFieldMapping(ESField esField) {
        Map<String, Object> map = new HashMap<>();
        map.put("type",esField.type().toString().toLowerCase());
        map.put("store",esField.store());
        map.put("index",esField.index());
        if (EmptyUtil.isNotEmpty(esField.format())){
            map.put("format",esField.format());
        }
        return map;
    }
}
