package com.example.javaapiclient.enums;

import java.io.Serializable;

public enum ESSortEnum implements Serializable {
    /**
     * 正序
     */
    ASC,
    /**
     * 倒序
     */
    DESC;
}
