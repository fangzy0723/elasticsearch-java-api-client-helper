package com.example.javaapiclient.annotations;

import com.example.javaapiclient.enums.ESAnalyzerType;
import com.example.javaapiclient.enums.ESFieldType;
import com.example.javaapiclient.enums.ESAnalyzerType;
import com.example.javaapiclient.enums.ESFieldType;

import java.lang.annotation.*;

/**
 * ES对象属性注解
 * 作用在成员变量，标记为文档的字段，并制定映射属性；
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Documented
@Inherited
public @interface ESField {

    /**
     * mapping中的字段名,为空时使用实体属性名
     */
    String name();
    /**
     * 属性类型
     */
    ESFieldType type() default ESFieldType.Keyword;

    /**
     * 是否索引
     */
    boolean index() default true;

    /**
     * 是否存储 默认不存储
     */
    boolean store() default false;

    /**
     * 指定字段使用搜索时的分词
     * ESAnalyzerType
     */
    ESAnalyzerType searchAnalyzer() default ESAnalyzerType.IK_MAX_WORD;

    /**
     * 分词器名称（索引分词/搜索分词）
     * ESAnalyzerType
     */
    ESAnalyzerType analyzer() default ESAnalyzerType.IK_MAX_WORD;

    /**
     * 字段类型为ESFieldType.Date时生效，多个日志格式之间||隔开
     */
    String format() default "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis||yyyy-MM-dd'T'HH:mm:ss.SSSZ||yyyy-MM-dd'T'HH:mm:ss.SSS";

    /**
     * text类型是否创建completion类型子字段，用于搜索补全
     * 支持前缀、模糊、正则查询，构建成FST整个加载进堆内存，消耗内存
     * 非text类型不起作用
     */
    boolean completion() default false;

    /**
     * text类型是否创建search_as_you_type类型子字段，用于搜索补全
     * 支持前缀补全和中缀补全,默认创建二元词组（_2gram）和三元词组（_3gram）
     * 非text类型不起作用
     */
    boolean searchAsYouType() default false;

    /**
     * 子类型对象
     */
    Class<?>[] childType() default {};
}

