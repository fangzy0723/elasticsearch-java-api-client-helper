package com.example.javaapiclient.strategy.templates;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service(value = "dynamicTemplatesDate")
public class DateDynamicTemplatesServiceImpl implements DynamicTemplatesService {

    @Override
    public Map<String, Object> getDynamicTemplates() {
        Map<String, Object> map = new HashMap<>();

        Map<String, Object> mapMapping = new HashMap<>();
        mapMapping.put("type","date");
        mapMapping.put("format","yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis||yyyy-MM-dd'T'HH:mm:ss.SSSZ||yyyy-MM-dd'T'HH:mm:ss.SSS");

        Map<String, Object> mapName = new HashMap<>();
        mapName.put("match","date_*");
        mapName.put("mapping",mapMapping);

        map.put("strings_as_date",mapName);
        return map;
    }
}
