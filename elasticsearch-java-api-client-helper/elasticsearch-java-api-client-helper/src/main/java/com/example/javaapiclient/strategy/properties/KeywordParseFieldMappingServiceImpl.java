package com.example.javaapiclient.strategy.properties;

import com.example.javaapiclient.annotations.ESField;
import com.example.javaapiclient.enums.ESFieldType;
import com.example.javaapiclient.utils.EmptyUtil;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service(value = "parseFieldMappingKeyword")
public class KeywordParseFieldMappingServiceImpl implements ParseFieldMappingService {
    @Override
    public Map<String, Object> getFieldMapping(ESField esField) {
        Map<String, Object> map = new HashMap<>();
        if (EmptyUtil.isEmpty(esField)){
            map.put("type", ESFieldType.Keyword.toString().toLowerCase());
        }else{
            map.put("type",esField.type().toString().toLowerCase());
            map.put("store",esField.store());
            map.put("index",esField.index());
        }
        return map;
    }
}
