package com.example.javaapiclient.strategy.aggregation;

import co.elastic.clients.elasticsearch._types.aggregations.Aggregation;
import com.example.javaapiclient.entity.common.AggregationParamEntity;
import com.example.javaapiclient.exception.BizException;

public interface ParseAggregationService {

    Aggregation parseAggregation(AggregationParamEntity param) throws BizException;
}
