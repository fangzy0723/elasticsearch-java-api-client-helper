package com.example.javaapiclient.strategy.templates;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service(value = "dynamicTemplatesKeyWord")
public class KeyWordDynamicTemplatesServiceImpl implements DynamicTemplatesService {

    @Override
    public Map<String, Object> getDynamicTemplates() {
        Map<String, Object> map = new HashMap<>();

        Map<String, Object> mapMapping = new HashMap<>();
        mapMapping.put("type","keyword");


        Map<String, Object> mapName = new HashMap<>();
        mapName.put("match_mapping_type","string");
        mapName.put("match","keyword_*");
        mapName.put("mapping",mapMapping);

        map.put("strings_as_keywords",mapName);
        return map;
    }
}
