package com.example.javaapiclient.strategy.templates;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service(value = "dynamicTemplatesInteger")
public class IntegerDynamicTemplatesServiceImpl implements DynamicTemplatesService {

    @Override
    public Map<String, Object> getDynamicTemplates() {
        Map<String, Object> map = new HashMap<>();

        Map<String, Object> mapMapping = new HashMap<>();
        mapMapping.put("type","integer");

        Map<String, Object> mapName = new HashMap<>();
        mapName.put("match","int_*");
        mapName.put("mapping",mapMapping);

        map.put("integer_as_long",mapName);
        return map;
    }
}
