package com.example.javaapiclient.strategy.templates;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service(value = "dynamicTemplatesText")
public class TextDynamicTemplatesServiceImpl implements DynamicTemplatesService {

    @Override
    public Map<String, Object> getDynamicTemplates() {
        Map<String, Object> map = new HashMap<>();

        Map<String, Object> mapKeyword = new HashMap<>();
        mapKeyword.put("type","keyword");
        mapKeyword.put("ignore_above","256");

        Map<String, Object> mapFields = new HashMap<>();
        mapFields.put("keyword",mapKeyword);

        Map<String, Object> mapMapping = new HashMap<>();
        mapMapping.put("type","text");
        mapMapping.put("analyzer","ik_max_word");
        mapMapping.put("search_analyzer","ik_max_word");
        mapMapping.put("fields",mapFields);


        Map<String, Object> mapName = new HashMap<>();
        mapName.put("match_mapping_type","string");
        mapName.put("match","text_*");
        mapName.put("mapping",mapMapping);

        map.put("string_as_text",mapName);
        return map;
    }
}
