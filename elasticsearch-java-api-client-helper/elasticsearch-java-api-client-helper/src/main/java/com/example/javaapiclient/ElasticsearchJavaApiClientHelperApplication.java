package com.example.javaapiclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ElasticsearchJavaApiClientHelperApplication {

    public static void main(String[] args) {
        SpringApplication.run(ElasticsearchJavaApiClientHelperApplication.class, args);
    }

}
