package com.example.javaapiclient.strategy.aggregation;

import co.elastic.clients.elasticsearch._types.aggregations.Aggregation;
import co.elastic.clients.elasticsearch._types.aggregations.CalendarInterval;
import co.elastic.clients.elasticsearch._types.aggregations.FieldDateMath;
import com.example.javaapiclient.entity.common.AggregationParamEntity;
import com.example.javaapiclient.exception.BizException;
import com.example.javaapiclient.utils.EmptyUtil;
import com.example.javaapiclient.utils.SpringContextUtil;
import org.springframework.stereotype.Service;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@Service(value = "dateHistogramParseAggregation")
public class DateHistogramParseAggregationServiceImpl implements ParseAggregationService {
    @Override
    public Aggregation parseAggregation(AggregationParamEntity param) throws BizException {

        if (EmptyUtil.isEmpty(param.getStartDate())){
            throw new BizException(0,"开始日期不能为空");
        }

        if (EmptyUtil.isEmpty(param.getEndDate())){
            throw new BizException(0,"结束日期不能为空");
        }

        if (EmptyUtil.isEmpty(param.getSubAgg())){
            return Aggregation.of(aggregation -> aggregation.dateHistogram(
                    dateHistogramAggregation -> dateHistogramAggregation
                            .field(param.getField())
                            .calendarInterval(CalendarInterval.Day)
                            .format("yyyy-MM-dd")
                            .keyed(true)
                            .extendedBounds(extendedBounds->extendedBounds
                                    .min(FieldDateMath.of(fdm->fdm.expr(param.getStartDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")))))
                                    .max(FieldDateMath.of(fdm->fdm.expr(param.getEndDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")))))
                            )
            ));
        }

        Map<String, Aggregation> subAggregationMap = new HashMap<>();

        for (AggregationParamEntity e:param.getSubAgg()) {
            ParseAggregationService service = (ParseAggregationService) SpringContextUtil.getBean(e.getType().toString()+"ParseAggregation");
            subAggregationMap.put(e.getName(),service.parseAggregation(e));
        }
        return Aggregation.of(aggregation -> aggregation.dateHistogram(
                dateHistogramAggregation -> dateHistogramAggregation
                        .field(param.getField())
                        .calendarInterval(CalendarInterval.Day)
                        .format("yyyy-MM-dd")
                        .keyed(true)
                        .extendedBounds(extendedBounds->extendedBounds
                                .min(FieldDateMath.of(fdm->fdm.expr(param.getStartDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")))))
                                .max(FieldDateMath.of(fdm->fdm.expr(param.getEndDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")))))
                        )
        ).aggregations(subAggregationMap));
    }
}
