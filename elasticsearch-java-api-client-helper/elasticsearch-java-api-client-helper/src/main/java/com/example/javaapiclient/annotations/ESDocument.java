package com.example.javaapiclient.annotations;

import java.lang.annotation.*;

/**
 * ES对象实体注解
 * 注解作用在类上，标记实体类为文档对象
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface ESDocument {

    //索引库名称 必须配置  和indexAliasesName 不能同时为空,不能相同
    String indexName();

    //索引别名 查询时刻使用别名查询  和indexName 不能同时为空,不能相同
    String indexAliasesName() default "";

    //默认分片数5
    int shards() default 5;

    //默认副本数1
    int replicas() default 1;

    //刷新间隔
    String refreshInterval() default "1s";

    //是否创建索引
    boolean createIndex() default true;
}
