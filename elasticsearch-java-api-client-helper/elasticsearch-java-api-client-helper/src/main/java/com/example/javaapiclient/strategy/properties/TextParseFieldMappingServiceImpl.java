package com.example.javaapiclient.strategy.properties;

import com.example.javaapiclient.annotations.ESField;
import com.example.javaapiclient.strategy.properties.ParseFieldMappingService;
import com.example.javaapiclient.utils.EmptyUtil;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service(value = "parseFieldMappingText")
public class TextParseFieldMappingServiceImpl implements ParseFieldMappingService {
    @Override
    public Map<String, Object> getFieldMapping(ESField esField) {
        Map<String, Object> map = new HashMap<>();
        map.put("type",esField.type().toString().toLowerCase());
        map.put("store",esField.store());
        map.put("index",esField.index());
        if (EmptyUtil.isNotEmpty(esField.analyzer()) && EmptyUtil.isNotEmpty(esField.analyzer().getCode())){
            map.put("analyzer",esField.analyzer().getCode());
        }
        if (EmptyUtil.isNotEmpty(esField.searchAnalyzer()) && EmptyUtil.isNotEmpty(esField.searchAnalyzer().getCode())){
            map.put("search_analyzer",esField.searchAnalyzer().getCode());
        }

        Map<String, Object> fieldsKeywordMap = new HashMap<>();
        fieldsKeywordMap.put("type","keyword");
        fieldsKeywordMap.put("ignore_above",256);

        Map<String, Object> fieldsMap = new HashMap<>();
        fieldsMap.put("keyword",fieldsKeywordMap);

        if (esField.completion()){
            Map<String, Object> fieldsCompletionMap = new HashMap<>();
            fieldsCompletionMap.put("type","completion");
            fieldsCompletionMap.put("analyzer",esField.analyzer().getCode());
            fieldsMap.put("completion",fieldsCompletionMap);
        }

        if (esField.searchAsYouType()){
            Map<String, Object> fieldSearchAsYouTypeMap = new HashMap<>();
            fieldSearchAsYouTypeMap.put("type","search_as_you_type");
            fieldSearchAsYouTypeMap.put("analyzer",esField.analyzer().getCode());
            fieldsMap.put("search_as_you_type",fieldSearchAsYouTypeMap);
        }

        map.put("fields",fieldsMap);
        return map;
    }
}
