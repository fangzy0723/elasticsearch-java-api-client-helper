package com.example.javaapiclient.strategy.templates;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service(value = "dynamicTemplatesDouble")
public class DoubleDynamicTemplatesServiceImpl implements DynamicTemplatesService {

    @Override
    public Map<String, Object> getDynamicTemplates() {
        Map<String, Object> map = new HashMap<>();

        Map<String, Object> mapMapping = new HashMap<>();
        mapMapping.put("type","double");

        Map<String, Object> mapName = new HashMap<>();
        mapName.put("match","double_*");
        mapName.put("match_mapping_type","double");
        mapName.put("mapping",mapMapping);

        map.put("float_as_double",mapName);
        return map;
    }
}
