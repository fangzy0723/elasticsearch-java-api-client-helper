package com.example.javaapiclient.strategy.aggregation;

import co.elastic.clients.elasticsearch._types.aggregations.Aggregation;
import com.example.javaapiclient.entity.common.AggregationParamEntity;
import com.example.javaapiclient.exception.BizException;
import com.example.javaapiclient.utils.EmptyUtil;
import com.example.javaapiclient.utils.SpringContextUtil;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.Map;

@Service(value = "percentileRanksParseAggregation")
public class PercentileRanksParseAggregationServiceImpl implements ParseAggregationService {
    @Override
    public Aggregation parseAggregation(AggregationParamEntity param) throws BizException {
        if (EmptyUtil.isEmpty(param.getPercentileRanksValues())){
            throw new BizException(0,"饼状图百分位值区间不能为空");
        }

        if (EmptyUtil.isEmpty(param.getSubAgg())){
            return Aggregation.of(aggregation -> aggregation.percentileRanks(
                    percentileRanksAggregation -> percentileRanksAggregation
                            .missing(0)
                            .keyed(true)
                            .field(param.getField())
                            .values(param.getPercentileRanksValues())
            ));
        }

        Map<String, Aggregation> subAggregationMap = new HashMap<>();

        for (AggregationParamEntity e:param.getSubAgg()) {
            ParseAggregationService service = (ParseAggregationService) SpringContextUtil.getBean(e.getType().toString()+"ParseAggregation");
            subAggregationMap.put(e.getName(),service.parseAggregation(e));
        }
        return Aggregation.of(aggregation -> aggregation.percentileRanks(
                percentileRanksAggregation -> percentileRanksAggregation
                        .missing(0)
                        .keyed(true)
                        .field(param.getField())
                        .values(param.getPercentileRanksValues())
        ).aggregations(subAggregationMap));
    }
}
