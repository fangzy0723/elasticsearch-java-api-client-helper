package com.example.javaapiclient.strategy.templates;

import java.util.Map;

public interface DynamicTemplatesService {

    Map<String,Object> getDynamicTemplates();
}
