package com.example.javaapiclient.service.impl;

import co.elastic.clients.elasticsearch._types.aggregations.Aggregate;
import co.elastic.clients.elasticsearch._types.query_dsl.Query;
import co.elastic.clients.elasticsearch.core.SearchRequest;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import com.example.javaapiclient.entity.UserInfoEntity;
import com.example.javaapiclient.entity.common.AggregationParamEntity;
import com.example.javaapiclient.entity.common.BulkResponseResult;
import com.example.javaapiclient.exception.BizException;
import com.example.javaapiclient.repository.ElasticsearchTemplate;
import com.example.javaapiclient.service.TestIndexAutoService;
import com.github.pagehelper.PageInfo;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Map;

@Service
public class TestIndexAutoServiceImpl implements TestIndexAutoService {

    @Resource
    private ElasticsearchTemplate<UserInfoEntity,String> elasticsearchTemplate;

    /**
     * 插入或更新文档，全量覆盖
     * id不存在插入文档
     * id存在更新文档
     * @param param 数据对象
     * @return true：插入成功 false：插入失败
     * @throws BizException 自定义异常
     */
    @Override
    public boolean saveOrUpdateCover(UserInfoEntity param) throws BizException {
        return elasticsearchTemplate.saveOrUpdateCover(param);
    }

    /**
     * 批量执行插入或修改操作，全量覆盖
     * id不存在插入文档
     * id存在更新文档
     * @param paramList
     * @return
     * @throws BizException
     */
    @Override
    public List<BulkResponseResult> saveOrUpdateCover(List<UserInfoEntity> paramList) throws BizException {
        return elasticsearchTemplate.saveOrUpdateCover(paramList);
    }

    @Override
    public List<BulkResponseResult> upsertBulkCover(List<UserInfoEntity> paramList) throws BizException {
        return elasticsearchTemplate.upsertBulkCover(paramList);
    }

    @Override
    public boolean deleteById(String id) throws BizException {
        return elasticsearchTemplate.deleteById(id,UserInfoEntity.class);
    }

    @Override
    public List<BulkResponseResult> deleteByIds(List<String> ids) throws BizException {
        return elasticsearchTemplate.deleteByIds(ids,UserInfoEntity.class);
    }

    @Override
    public Long deleteByQuery(Query query) throws BizException {
        return elasticsearchTemplate.deleteByQuery(query,UserInfoEntity.class);
    }

    @Override
    public UserInfoEntity getById(String id) throws BizException {
        return elasticsearchTemplate.getById(id,UserInfoEntity.class);
    }

    @Override
    public List<UserInfoEntity> mGetById(List<String> ids) throws BizException {
        return elasticsearchTemplate.mGetById(ids,UserInfoEntity.class);
    }

    @Override
    public boolean exists(String id) throws BizException {
        return elasticsearchTemplate.exists(id,UserInfoEntity.class);
    }

    /**
     * 根据id修改，只修改字段值不为null的字段
     *
     * @param param
     */
    @Override
    public boolean updateById(UserInfoEntity param) throws BizException {
        return elasticsearchTemplate.updateById(param);
    }

    /**
     * 根据查询条件修改
     * @param query 查询参数
     * @param updateParams 更新参数(如果有多个条件、条件之间是并且关系)
     * @return
     * @throws BizException
     */
    @Override
    public Long updateByQuery(Query query,Map<String,Object> updateParams) throws BizException {
        return elasticsearchTemplate.updateByQuery(query,UserInfoEntity.class,updateParams);
    }

    @Override
    public List<UserInfoEntity> searchList(SearchRequest.Builder searchRequestBuilder) throws BizException {
        return elasticsearchTemplate.searchList(searchRequestBuilder,UserInfoEntity.class);
    }

    @Override
    public PageInfo<UserInfoEntity> searchPage(SearchRequest.Builder searchRequestBuilder, int pageNo, int pageSize) throws BizException {
        return elasticsearchTemplate.searchPage(searchRequestBuilder,pageNo,pageSize,UserInfoEntity.class);
    }

    @Override
    public List<String> completionSuggest(String fieldName, String fieldValue) throws BizException {
        return elasticsearchTemplate.completionSuggest(fieldName,fieldValue,UserInfoEntity.class);
    }

    @Override
    public List<String> completionSearchAsYouType(String fieldName, String fieldValue) throws BizException {
        return elasticsearchTemplate.completionSearchAsYouType(fieldName,fieldValue,UserInfoEntity.class);
    }

    @Override
    public Map<String,Object> scroll(SearchRequest.Builder builder,String scrollId) throws BizException {
        return elasticsearchTemplate.scroll(builder,scrollId,UserInfoEntity.class);
    }

    @Override
    public Map<String,Object> scroll(SearchRequest.Builder builder,String scrollId,String time) throws BizException {
        return elasticsearchTemplate.scroll(builder,scrollId,UserInfoEntity.class,time);
    }

    /**
     * 根据查询条件查询原始结果,不分页
     * @param searchRequestBuilder
     * @return
     * @throws BizException
     */
    @Override
    public SearchResponse searchOrigin(SearchRequest.Builder searchRequestBuilder) throws BizException {
        return elasticsearchTemplate.searchOrigin(searchRequestBuilder,UserInfoEntity.class);
    }

    /**
     * 根据查询条件统计总条数
     * @param query
     * @return
     * @throws BizException
     */
    @Override
    public Long countTotal(Query query) throws BizException {
        return elasticsearchTemplate.countTotal(query,UserInfoEntity.class);
    }

    @Override
    public Map<String, Aggregate> aggregation(Query query, List<AggregationParamEntity> aggParam) throws BizException {
        return elasticsearchTemplate.aggregation(query,aggParam,UserInfoEntity.class);
    }

}
