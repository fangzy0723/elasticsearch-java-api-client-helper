package com.example.javaapiclient.service;

import co.elastic.clients.elasticsearch._types.*;
import co.elastic.clients.elasticsearch._types.aggregations.Aggregate;
import co.elastic.clients.elasticsearch._types.aggregations.Aggregation;
import co.elastic.clients.elasticsearch._types.query_dsl.Query;
import co.elastic.clients.elasticsearch.core.SearchRequest;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import co.elastic.clients.json.JsonData;
import com.alibaba.fastjson.JSON;
import com.example.javaapiclient.entity.*;
import com.example.javaapiclient.entity.common.AggregationParamEntity;
import com.example.javaapiclient.enums.ESAggregationType;
import com.example.javaapiclient.exception.BizException;
import com.example.javaapiclient.utils.EmptyUtil;
//import org.elasticsearch.client.Node;
//import org.elasticsearch.client.RestHighLevelClient;
//import org.elasticsearch.index.query.QueryBuilder;
//import org.elasticsearch.index.query.QueryBuilders;
//import org.elasticsearch.search.aggregations.Aggregation;
//import org.elasticsearch.search.builder.SearchSourceBuilder;
//import org.elasticsearch.search.sort.SortOrder;
import jakarta.annotation.Resource;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.*;

@SpringBootTest
public class TestServiceTest {


    @Resource
    private TestIndexAutoService testIndexAutoService;
//    @Test
//    void saveNestedIndex() throws BizException {
//        ChinaCityInfoModel chinaCityInfoModel = new ChinaCityInfoModel();
//
//        CityInfoModel cityInfoModel = new CityInfoModel();
//        cityInfoModel.setName("张三");
//        cityInfoModel.setPostCodeList(Arrays.asList("a","b","c","d"));
//        cityInfoModel.setStatusList(Arrays.asList(1,2,3,4));
//
//        AreaInfoModel a1 = new AreaInfoModel();
//        a1.setName("朝阳");
//        a1.setPostCode("10001");
//        a1.setStatus("1");
//
//        cityInfoModel.setArea(a1);
//
//
//        AreaInfoModel a2 = new AreaInfoModel();
//        a2.setName("海淀");
//        a2.setPostCode("10002");
//        a2.setStatus("2");
//
//        AreaInfoModel a3 = new AreaInfoModel();
//        a3.setName("大兴");
//        a3.setPostCode("10003");
//        a3.setStatus("3");
//
//        AreaInfoModel a4 = new AreaInfoModel();
//        a4.setName("房山");
//        a4.setPostCode("10004");
//        a4.setStatus("4");
//
//        List<AreaInfoModel> list = new ArrayList<>();
//        list.add(a2);
//        list.add(a3);
//        list.add(a4);
//        cityInfoModel.setAreaList(list);
//
//        ProvinceInfoModel provinceInfoModel = new ProvinceInfoModel();
//        provinceInfoModel.setCity(cityInfoModel);
//        provinceInfoModel.setName("北京市");
//        provinceInfoModel.setPostCode("100000");
//        provinceInfoModel.setStatus("0");
//        chinaCityInfoModel.setProvince(provinceInfoModel);
//
//
//        System.out.println(testNestedIndexAutoService.saveOrUpdateCover(chinaCityInfoModel));
//
//    }
//
//    @Test
//    void getByIdNestedIndex() throws BizException {
//        ChinaCityInfoModel ch = testNestedIndexAutoService.getById("Evc0PoEBrx3CyPdyJUCe");
//        System.out.println(ch);
//    }
//

    @Test
    void save() throws BizException {

        UserInfoEntity userInfo = new UserInfoEntity();
        userInfo.setId("1");
        userInfo.setUserName("张三");
        userInfo.setUserAge(20);
        userInfo.setBirthDate(LocalDateTime.now());

        System.out.println(testIndexAutoService.saveOrUpdateCover(userInfo));

    }

    @Test
    void batchInsert() throws BizException {

        UserInfoEntity userInfo = new UserInfoEntity();
        userInfo.setId("1");
        userInfo.setUserName("张三");
        userInfo.setUserAge(20);
        userInfo.setBirthDate(LocalDateTime.now());
        userInfo.setTextCompletion("北京天安门");

        UserInfoEntity userInfo2 = new UserInfoEntity();
        userInfo2.setId("2");
        userInfo2.setUserName("张三2");
        userInfo2.setUserAge(22);
        userInfo2.setBirthDate(LocalDateTime.now());
        userInfo2.setTextCompletion("中国北京大学");

        UserInfoEntity userInfo3 = new UserInfoEntity();
        userInfo3.setId("3");
        userInfo3.setUserName("张三3");
        userInfo3.setUserAge(23);
        userInfo3.setBirthDate(LocalDateTime.now());
        userInfo3.setTextCompletion("清华大学");


        List<UserInfoEntity> list = Arrays.asList(userInfo,userInfo2,userInfo3);

         System.out.println(testIndexAutoService.saveOrUpdateCover(list));

    }

    @Test
    void upsertBulkCover() throws BizException {

        UserInfoEntity userInfo = new UserInfoEntity();
        userInfo.setId("1");
        userInfo.setUserName("张三_003");
        userInfo.setUserAge(20);
        userInfo.setBirthDate(LocalDateTime.now());

        UserInfoEntity userInfo4 = new UserInfoEntity();
        userInfo4.setId("4");
        userInfo4.setUserName("张三4");
        userInfo4.setUserAge(24);
        userInfo4.setBirthDate(LocalDateTime.now());

        List<UserInfoEntity> list = Arrays.asList(userInfo,userInfo4);

        System.out.println(testIndexAutoService.upsertBulkCover(list));

    }

    @Test
    void deleteById() throws BizException {
        System.out.println(testIndexAutoService.deleteById("3"));
    }

    @Test
    void deleteByIds() throws BizException {
        System.out.println(testIndexAutoService.deleteByIds(Arrays.asList("1","2")));
    }


    @Test
    void deleteByQuery() throws BizException {
        Query.Builder queryBuilder = new Query.Builder();
        queryBuilder.range(rangeQuery->rangeQuery.field("user_age").gte(JsonData.of(20)));
        System.out.println(testIndexAutoService.deleteByQuery(queryBuilder.build()));
    }

    @Test
    void getById() throws BizException {
        System.out.println(testIndexAutoService.getById("1"));
    }

    @Test
    void mGetById() throws BizException {
        System.out.println(testIndexAutoService.mGetById(Arrays.asList("4")));
    }

    @Test
    void exists() throws BizException {
        System.out.println(testIndexAutoService.exists("1"));
    }

    @Test
    void updateById() throws BizException {

        UserInfoEntity userInfo = new UserInfoEntity();
        userInfo.setId("1");
        userInfo.setUserName("张三1");
        userInfo.setUserAge(21);
        System.out.println(testIndexAutoService.updateById(userInfo));

    }

    @Test
    void updateByQuery() throws BizException {
        Query.Builder builder = new Query.Builder();
        builder.range(rangeQuery->rangeQuery.field("user_age").gte(JsonData.of("20")));

        Map<String,Object> map = new HashMap<>();
        map.put("user_age",29);
        map.put("user_name","hehe01");
        map.put("birth_date",LocalDateTime.now());
        System.out.println(testIndexAutoService.updateByQuery(builder.build(),map));
    }

    @Test
    void searchOrigin() throws BizException {
        Query.Builder builder = new Query.Builder();
        builder.range(rangeQuery->rangeQuery.field("user_age").gte(JsonData.of("20")));

        SearchRequest.Builder searchRequest = new SearchRequest.Builder();
        searchRequest.query(builder.build());
        System.out.println(testIndexAutoService.searchOrigin(searchRequest));
    }

    @Test
    void countTotal() throws BizException {
        Query.Builder builder = new Query.Builder();
        builder.range(rangeQuery->rangeQuery.field("user_age").gte(JsonData.of("20")));
        System.out.println(testIndexAutoService.countTotal(builder.build()));
    }

    @Test
    void searchList() throws BizException {

        Query.Builder builder = new Query.Builder();
        builder.range(rangeQuery->rangeQuery.field("user_age").gte(JsonData.of("20")));

        SearchRequest.Builder searchRequest = new SearchRequest.Builder();
        searchRequest.query(builder.build());
        System.out.println(testIndexAutoService.searchList(searchRequest));
    }

    @Test
    void searchPage() throws BizException {
        Query.Builder builder = new Query.Builder();
        builder.range(rangeQuery->rangeQuery.field("user_age").gte(JsonData.of("20")));

        SearchRequest.Builder searchRequest = new SearchRequest.Builder();
        searchRequest.query(builder.build());
        System.out.println(testIndexAutoService.searchPage(searchRequest,1,2));
    }


    @Test
    void completionSuggest() throws BizException {
        System.out.println(testIndexAutoService.completionSuggest("text_completion","北京"));
    }

    @Test
    void completionSearchAsYouType() throws BizException {
        System.out.println(testIndexAutoService.completionSearchAsYouType("text_completion","华大"));
    }

    @Test
    void scroll() throws BizException {

        Query.Builder builder = new Query.Builder();
        builder.range(rangeQuery->rangeQuery.field("user_age").gte(JsonData.of("20")));

        SearchRequest.Builder searchBuilder = new SearchRequest.Builder();
        searchBuilder.query(builder.build());
        searchBuilder.size(1);


        SortOptions.Builder sortOptions = new SortOptions.Builder();

        FieldSort.Builder fieldSort = new FieldSort.Builder();
        fieldSort.field("id");
        fieldSort.order(SortOrder.Desc);
        sortOptions.field(fieldSort.build());
        searchBuilder.sort(sortOptions.build());


//        queryBuilder.searchAfter(new Object[]{});
        Map<String, Object> map = testIndexAutoService.scroll(searchBuilder, "");
        System.out.println(JSON.toJSONString(map));
        while (EmptyUtil.isNotEmpty(map) && EmptyUtil.isNotEmpty(map.getOrDefault("result",""))){
            map = testIndexAutoService.scroll(null, (String) map.getOrDefault("scrollId",""));
            System.out.println(JSON.toJSONString(map));
        }
    }

    @Test
    void aggregation() throws BizException {
        Query.Builder builder = new Query.Builder();
        builder.range(rangeQuery->rangeQuery.field("user_age").gte(JsonData.of("20")));


        List<AggregationParamEntity> aggParam = new ArrayList<>();
//        aggParam.add(AggregationParamEntity.builder().name("user_name_agg").type(ESAggregationType.terms).field("user_name.keyword").build());
//        aggParam.add(AggregationParamEntity.builder().name("user_name_agg").type(ESAggregationType.terms).field("user_name.keyword").subAgg(Arrays.asList(AggregationParamEntity.builder().name("user_age_avg_agg").type(ESAggregationType.avg).field("user_age").build())).build());
//        aggParam.add(AggregationParamEntity.builder().name("user_name_agg").type(ESAggregationType.terms).field("user_name").subAgg(Arrays.asList(AggregationParamEntity.builder().name("user_age_avg_agg").type(ESAggregationType.avg).field("user_age").build(),AggregationParamEntity.builder().name("weight_max_agg").type(ESAggregationType.max).field("weight").build())).build());
//        aggParam.add(AggregationParamEntity.builder().name("weight_avg_agg").type(ESAggregationType.avg).field("weight").build());
        Map<String, Aggregate> map = testIndexAutoService.aggregation(builder.build(), aggParam);
        System.out.println(map);
    }
}
